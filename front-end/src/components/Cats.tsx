import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';

type CatApiBody = {
  id: string;
  created_at: string;
  tags: string[];
  url: string;
};

const strings = {
  buttonText: 'Get Random Cat',
  imgAlt: 'cute cat',
  title: 'Cute Cats!',
};

const Cats: React.FC = () => {
  const [catUrl, setCatUrl] = useState<string>(
    'https://cataas.com/cat/5f39a7075bc3fa00104444c3',
  );

  const getCat = async () => {
    const response = await fetch('https://cataas.com/cat?json=true');
    const cat: CatApiBody = await response.json();
    setCatUrl(`https://cataas.com${cat.url}`);
  };

  return (
    <div className="d-flex align-items-center flex-column m-2">
      <h1>{strings.title}</h1>
      <img
        className="mt-2"
        src={catUrl}
        alt={strings.imgAlt}
        style={{ width: 800, height: 600 }}
      />
      <Button className="mt-4" variant="primary" onClick={getCat}>
        {strings.buttonText}
      </Button>
    </div>
  );
};

export default Cats;
