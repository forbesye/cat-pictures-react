import React from 'react';
import Cats from './components/Cats';

const App: React.FC = () => {
  return (
    <div>
      <Cats />
    </div>
  );
};

export default App;
